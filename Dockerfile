FROM node:16-alpine as node-app

COPY . /app
WORKDIR /app
RUN npm i --no-optional
RUN npm run build

FROM node:16-alpine

COPY docker-entrypoint.sh /app/docker-entrypoint.sh
COPY --from=node-app --chown=node:node /app/dist /app
WORKDIR /app
ENV NODE_ENV=production
RUN npm i
RUN npm audit fix
RUN npx prisma generate
USER node
ENTRYPOINT [ "/app/docker-entrypoint.sh" ]
