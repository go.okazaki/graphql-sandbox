DOCKER_REGISTRY ?= localhost
VERSION_TAG ?= latest
IMAGE_TAG = $(DOCKER_REGISTRY)/graphql-sandbox:$(VERSION_TAG)

all: build

build:
	npm i
	npm run build
	npm run lint:fix

test:
	npx prisma db push
	npx prisma db seed
	npm test

install:
	npx prisma migrate dev
	docker build -t $(IMAGE_TAG) .

clean:
	rm -Rf dist node_modules src/generated test/src/generated
