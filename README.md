# GraphQL Sandbox

## Requirements

- GNU Make
- NodeJS
- Docker

## Build

```shell
make
```

## Development

```shell
docker-compose up
```

- [GraphQL Endpoint](http://localhost:3000/graphql)

### Prisma

- Synchronize schema for development database

```shell
npx prisma db push
```

- Seeding for development database

```shell
npx prisma db seed
```
