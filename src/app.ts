import express from 'express'
import compression from 'compression'
import helmet from 'helmet'
import morgan from 'morgan'
import { execute } from 'graphql'
// https://github.com/graphql/express-graphql
import { graphqlHTTP } from 'express-graphql'
// https://github.com/jaydenseric/graphql-upload
import { graphqlUploadExpress } from 'graphql-upload'

import schema from './schema'
import { getContext } from './context'
import { fileRouter } from './file'
import { handleGraphQLErrors } from './core'

const app = express()
app.use(compression())
app.use(helmet())
app.use(
  morgan('tiny', {
    skip: (req, _res) => {
      return req.originalUrl.startsWith('/health')
    },
  }),
)
app.get('/health', (_req, res) => {
  res.setHeader('content-type', 'text/plain')
  res.send('OK')
})
app.use(
  '/graphql',
  graphqlUploadExpress({ maxFileSize: 100000000, maxFiles: 10 }),
  (req, res) =>
    graphqlHTTP({
      schema,
      context: getContext(req, res),
      customExecuteFn: async (args) => {
        const result = await execute(args)
        return handleGraphQLErrors(result)
      },
      graphiql: process.env.NODE_ENV !== 'production',
    })(req, res),
)
app.use(fileRouter)

export default app
