import bcrypt from 'bcrypt'
import { User as PUser } from '@prisma/client'
import {
  LoginPayload,
  MutationResolvers,
  QueryResolvers,
  User,
} from '../generated/graphql'
import { assertNotEmpty, ClientError } from '../core'
import { generateToken } from './token'
import { toUser } from '../user'

export const authQueryResolvers: QueryResolvers = {
  me: async (_parent, _args, context): Promise<User> => {
    const id = assertNotEmpty(context.user?.sub)
    const user = await context.prisma.user.findUnique({
      where: { id },
    })
    if (!user) {
      throw new ClientError(`User is undefined`)
    }
    return toUser(user)
  },
}

export const authMutationResolvers: MutationResolvers = {
  login: async (_parent, args, context): Promise<LoginPayload> => {
    const user = await context.prisma.user.findUnique({
      where: { email: args.input.email },
    })
    if (
      !user?.enabled ||
      !user?.password ||
      !bcrypt.compareSync(args.input.password, user?.password)
    ) {
      throw new ClientError(`Login failed`)
    }
    const token = generateToken({
      sub: user.id,
      role: user.role,
    })
    return {
      token,
    }
  },
  updateMe: async (_parent, args, context): Promise<User> => {
    const userId = assertNotEmpty(context.user?.sub)
    const result = await context.prisma.user.update({
      where: { id: userId },
      data: {
        name: args.input.name,
        followers: {
          connectOrCreate: args.input.addFollow
            ? {
                where: {
                  followerId_followId: {
                    followId: args.input.addFollow,
                    followerId: userId,
                  },
                },
                create: {
                  followId: args.input.addFollow,
                },
              }
            : undefined,
          delete: args.input.removeFollow
            ? {
                followerId_followId: {
                  followId: args.input.removeFollow,
                  followerId: userId,
                },
              }
            : undefined,
        },
        tags: {
          connectOrCreate: args.input.addTag
            ? {
                where: {
                  userId_tagId: {
                    tagId: args.input.addTag,
                    userId,
                  },
                },
                create: {
                  tagId: args.input.addTag,
                },
              }
            : undefined,
          delete: args.input.removeTag
            ? {
                userId_tagId: {
                  tagId: args.input.removeTag,
                  userId,
                },
              }
            : undefined,
        },
      },
    })
    if (!result) {
      throw new ClientError(`User id:${userId} not found`)
    }
    return toUser(result)
  },
}
