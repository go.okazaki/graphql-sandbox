import crypto, { KeyPairSyncResult } from 'crypto'
import jwt, { JwtPayload } from 'jsonwebtoken'

export interface Payload extends JwtPayload {
  sub: string
  role: string
}

let privateKey: string | undefined
let publicKey: string | undefined

export function updateKeyPair(): KeyPairSyncResult<string, string> {
  const result = crypto.generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem',
    },
    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'pem',
    },
  })
  privateKey = result.privateKey
  publicKey = result.publicKey
  return result
}

function getPrivateKey(): string {
  let key
  if (privateKey) {
    key = privateKey
  } else {
    key = updateKeyPair().privateKey
  }
  return key
}

export function getPublicKey(): string {
  let key
  if (publicKey) {
    key = publicKey
  } else {
    key = updateKeyPair().publicKey
  }
  return key
}

export function generateToken(payload: Payload): string {
  const privateKey = getPrivateKey()
  const encoded = jwt.sign(payload, privateKey, { algorithm: 'RS256' })
  return encoded
}

export function verifyToken(token: string): Payload | undefined {
  const publicKey = getPublicKey()
  let decoded
  try {
    decoded = jwt.verify(token, publicKey, { algorithms: ['RS256'] })
  } catch (e) {
    console.warn(e)
  }
  let payload: Payload | undefined
  if (typeof decoded === 'string') {
    payload = JSON.parse(decoded)
  } else if (decoded) {
    payload = decoded as Payload
  }
  return payload
}
