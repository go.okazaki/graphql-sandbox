import { Request, Response } from 'express'
import { Prisma, PrismaClient } from '@prisma/client'
import { Payload, verifyToken } from './auth'

export interface Context {
  user: Payload | undefined
  req: Request
  res: Response
  prisma: PrismaClient
  isAuthenticated: () => boolean
  isAdmin: () => boolean
}

const prismaLogLevels: Array<Prisma.LogLevel> = [
  'query',
  // 'info',
  'warn',
  'error',
]
const prisma = new PrismaClient({
  log: prismaLogLevels,
})

export function getContext(req: Request, res: Response): Context {
  let user: Payload | undefined
  const token = req.headers.authorization
    ? req.headers.authorization.split(' ')[1]
    : undefined
  if (token) {
    user = verifyToken(token)
  }
  return {
    user,
    req,
    res,
    prisma,
    isAuthenticated: () => {
      return user ? true : false
    },
    isAdmin: () => {
      return user?.role === 'admin'
    },
  }
}
