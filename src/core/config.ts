import path from 'path'

export function getConfig() {
  const resourceBaseDir = path.resolve(process.cwd(), 'resources')

  return {
    resourceBaseDir,
  }
}
