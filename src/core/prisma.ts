// https://github.com/devoxa/prisma-relay-cursor-connection
import {
  Connection,
  ConnectionArguments,
  Edge,
  findManyCursorConnection,
  Options,
  PrismaFindManyArguments,
} from '@devoxa/prisma-relay-cursor-connection'
import { GraphQLResolveInfo } from 'graphql'
import { decodeBase64, encodeBase64 } from '.'

export type CursorConnection<T> = Connection<T, Edge<T>>

export const emptyConnection = {
  edges: [],
  totalCount: 0,
  pageInfo: {
    hasNextPage: false,
    hasPreviousPage: false,
  },
}

export async function toCursorConnection<
  R = {
    id: string
  },
  N = R,
  C = {
    id: string
  },
>(
  findMany: (args: PrismaFindManyArguments<C>) => Promise<R[]>,
  aggregate: () => Promise<number>,
  args?: ConnectionArguments,
  recordToNode = (record: R): N => record as unknown as N,
  getCursor = (record: R): C => ({ id: (record as any).id } as unknown as C),
  encodeCursor = (cursor: C): string => encodeBase64<C>(cursor),
  decodeCursor = (cursorString: string): C => decodeBase64<C>(cursorString),
  resolveInfo: GraphQLResolveInfo | null = null,
): Promise<CursorConnection<N>> {
  const newOptions: Options<R, C, N, Edge<N>> = {
    getCursor,
    encodeCursor,
    decodeCursor,
    recordToEdge: (record) => ({ node: recordToNode(record) }),
    resolveInfo,
  }
  return await findManyCursorConnection(findMany, aggregate, args, newOptions)
}
