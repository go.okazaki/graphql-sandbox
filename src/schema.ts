import path from 'path'
import { loadSchemaSync } from '@graphql-tools/load'
import { makeExecutableSchema } from '@graphql-tools/schema'
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader'
// https://github.com/jaydenseric/graphql-upload#class-graphqlupload
import { GraphQLUpload } from 'graphql-upload'
// https://github.com/confuser/graphql-constraint-directive
import { constraintDirective } from 'graphql-constraint-directive'
// https://github.com/maticzav/graphql-middleware
import { applyMiddleware } from 'graphql-middleware'
// https://github.com/maticzav/graphql-shield
import { not, rule, shield } from 'graphql-shield'

import { Resolvers } from './generated/graphql'
import { ClientError, getConfig } from './core'
import {
  userMutationResolvers,
  userQueryResolvers,
  userResolvers,
} from './user'
import { fileMutationResolvers, fileQueryResolvers } from './file'
import { tagMutationResolvers, tagQueryResolvers, tagResolvers } from './tag'
import { authMutationResolvers, authQueryResolvers } from './auth'

const resolvers: Resolvers = {
  Query: {
    ...authQueryResolvers,
    ...userQueryResolvers,
    ...fileQueryResolvers,
    ...tagQueryResolvers,
  },
  Mutation: {
    ...authMutationResolvers,
    ...userMutationResolvers,
    ...fileMutationResolvers,
    ...tagMutationResolvers,
  },
  User: {
    ...userResolvers,
  },
  Tag: {
    ...tagResolvers,
  },
  Upload: GraphQLUpload,
}

const schemaPath = path.join(getConfig().resourceBaseDir, '*schema.graphql')
const schema = makeExecutableSchema({
  typeDefs: [
    loadSchemaSync(schemaPath, {
      loaders: [new GraphQLFileLoader()],
    }),
  ],
  resolvers,
})

const isAuthenticated = rule({ cache: 'contextual' })(
  async (_parent, _args, context, _info) => {
    return context.isAuthenticated()
  },
)

const isAdmin = rule({ cache: 'contextual' })(
  async (_parent, _args, context, _info) => {
    return context.isAdmin()
  },
)

const permissions = shield(
  {
    Query: {
      '*': isAuthenticated,
    },
    Mutation: {
      '*': isAuthenticated,
      login: not(isAuthenticated),
      createUser: isAdmin,
    },
  },
  {
    fallbackError: new ClientError(`Not Authorized`),
  },
)

export default applyMiddleware(constraintDirective()(schema), permissions)
