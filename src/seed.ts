import { Prisma, PrismaClient } from '@prisma/client'
import bcrypt from 'bcrypt'

const prisma = new PrismaClient()
const saltRounds = 10
const salt = bcrypt.genSaltSync(saltRounds)

const users: Prisma.UserCreateInput[] = [
  {
    name: 'Sandbox',
    email: 'sandbox@example.com',
    password: bcrypt.hashSync('sandbox', salt),
    enabled: true,
    role: 'admin',
  },
]

async function main() {
  console.log(`Start seeding ...`)
  for (const user of users) {
    const result = await prisma.user.upsert({
      where: { email: user.email },
      create: user,
      update: user,
    })
    console.log(`Created user with id: ${result.id}`)
  }
  console.log(`Seeding finished.`)
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
