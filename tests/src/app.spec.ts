process.env.JWKS_URI = 'http://localhost'

import request from 'supertest'
import app from '../../src/app'

describe('app', () => {
  it('health check', async () => {
    await request(app).get('/health').expect(200)
  })
})
