import { testDocumentRequest } from '../util'
import {
  LoginDocument,
  LoginMutationVariables,
  MeDocument,
  MeQueryVariables,
} from '../generated/client'

describe('graphql', () => {
  it('mutation login', async () => {
    const loginResponse = await testDocumentRequest<LoginMutationVariables>(
      LoginDocument,
      {
        input: {
          email: 'sandbox@example.com',
          password: 'sandbox',
        },
      },
    )
    expect(loginResponse.statusCode).toBe(200)
    const login = loginResponse.body.data.login
    const token = login.token as string
    expect(token).not.toBeUndefined()

    const meResponse = await testDocumentRequest<MeQueryVariables>(
      MeDocument,
      {},
    ).auth(token, { type: 'bearer' })

    expect(meResponse.statusCode).toBe(200)
    const me = meResponse.body.data.me
    expect(me).not.toBeUndefined()
    expect(me.id).not.toBeUndefined()
  })
})
