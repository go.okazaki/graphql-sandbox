import jwt from 'jsonwebtoken'
import { generateToken, verifyToken } from '../../../src/auth/token'

describe('auth', () => {
  it('token', async () => {
    const payload = { sub: 'test', role: 'user' }
    const token = generateToken(payload)
    // console.log(token)
    const result = verifyToken(token)
    // console.log(result)
    expect(result?.sub).toBe(payload.sub)
    expect(result?.role).toBe(payload.role)
  })

  it('token fake', async () => {
    const fake = jwt.sign({ sub: 'test', role: 'admin' }, 'shhhhh')
    // console.log(fake)
    const result = verifyToken(fake)
    expect(result).toBeUndefined()
  })
})
