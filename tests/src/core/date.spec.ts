import assert from 'assert'
import { formatDate, parseDate } from '../../../src/core'

describe('date', () => {
  it('parseDate', async () => {
    const result = parseDate('20180820000000', 'yyyyMMddHHmmss')
    assert(result)
    expect(result.getFullYear()).toEqual(2018)
    expect(result.getMonth() + 1).toEqual(8)
    expect(result.getDate()).toEqual(20)
    expect(result.getHours()).toEqual(0)
    expect(result.getMinutes()).toEqual(0)
    expect(result.getSeconds()).toEqual(0)
    expect(result.getMilliseconds()).toEqual(0)
  })

  it('parseDate Asia/Tokyo', async () => {
    const result = parseDate('20180820000000', 'yyyyMMddHHmmss', 'Asia/Tokyo')
    assert(result)
    expect(result.getUTCFullYear()).toEqual(2018)
    expect(result.getUTCMonth() + 1).toEqual(8)
    expect(result.getUTCDate()).toEqual(19)
    expect(result.getUTCHours()).toEqual(15)
    expect(result.getUTCMinutes()).toEqual(0)
    expect(result.getUTCSeconds()).toEqual(0)
    expect(result.getUTCMilliseconds()).toEqual(0)
  })

  it('parseDate UTC', async () => {
    const result = parseDate('20180820000000', 'yyyyMMddHHmmss', 'UTC')
    assert(result)
    expect(result.getUTCFullYear()).toEqual(2018)
    expect(result.getUTCMonth() + 1).toEqual(8)
    expect(result.getUTCDate()).toEqual(20)
    expect(result.getUTCHours()).toEqual(0)
    expect(result.getUTCMinutes()).toEqual(0)
    expect(result.getUTCSeconds()).toEqual(0)
    expect(result.getUTCMilliseconds()).toEqual(0)
  })

  it('formatDate & parseDate', async () => {
    const date = new Date()
    let format = 'yyyy/MM/dd HH:mm:ss.SSS'
    let text = formatDate(date, format)
    let result = parseDate(text, format)
    expect(result).toEqual(date)

    text = formatDate(date, format, 'UTC')
    result = parseDate(text, format, 'UTC')
    expect(result).toEqual(date)

    date.setMilliseconds(0)
    format = "yyyyMMdd'T'HHmmssxx"
    text = formatDate(date, format)
    result = parseDate(text, format)
    expect(result).toEqual(date)
  })
})
